﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4
{
    public partial class SpyroInfo : ContentPage
    {
        public SpyroInfo()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left the Spyro page", "Ok");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Spyro page", "Ok");
        }

        //This function removes the button and displays the info about spyro
        private void spyro_click(object sender, EventArgs e)
        {

            Label label = spyro_button_info;
            Label label1 = spyro_description;
            Button removeButton = spyro_button;

            removeButton.IsVisible = !removeButton.IsVisible;
            spyro_button_info.IsVisible = !spyro_button_info.IsVisible;
            spyro_description.Text = "Spyro is a young purple male dragon. He is known for his ability to defeat enemies by breathing fire and charging at them, and his ability to glide to otherwise unreachable areas in the game world. Spyro is often accompanied by his best friend Sparx, a dragonfly.";
        }

        //This function changes the color of the background when the button is clicked
        void text_click(object sender, EventArgs e)
        {
            Random rand_Color = new Random();
            spyro_description.BackgroundColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
        }
    }
}