﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4
{
    public partial class SlyCooperInfo : ContentPage
    {
        public SlyCooperInfo()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left the Sly Cooper page", "Ok");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Sly Cooper page", "Ok");
        }

        //Button that removes the button and displays the info about crash
        private void sly_click(object sender, EventArgs e)
        {

            Label label = sly_button_info;
            Label label1 = sly_description ;
            Button removeButton = sly_button;

            removeButton.IsVisible = !removeButton.IsVisible;
            sly_button_info.IsVisible = !sly_button_info.IsVisible;
            sly_description.Text = "Sly is a raccoon who is descended from a long line of master thieves and uses his skills, with the help of his long-time friends Bentley and Murray, to pull off heists.";
        }

        //Function to change background color when the button is clicked
        void text_click(object sender, EventArgs e)
        {
            Random rand_Color = new Random();
            BackgroundColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
        }
    }
}