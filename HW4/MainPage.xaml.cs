﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace HW4
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<GameChar> GameCharList { get; set; }

        public MainPage()
        {
            InitializeComponent();
            PopulateList();
        }

        private void PopulateList()
        {
            GameCharList = new ObservableCollection<GameChar>()
            {
                new GameChar()
                {
                    Name = "Ratchet",
                    Type = "Lombax",
                    ImageSource = "ratchet.png"
                },

                new GameChar()
                {
                    Name = "Spyro",
                    Type = "Dragon",
                    ImageSource = "spyro.jpg"
                },

                new GameChar()
                {
                    Name = "Crash",
                    Type = "Bandicoot",
                    ImageSource = "crash.jpg"
                },

                new GameChar()
                {
                    Name = "Jak",
                    Type = "Human",
                    ImageSource = "Jak.jpg"
                },

                new GameChar()
                {
                    Name = "Sly Cooper",
                    Type = "Raccoon",
                    ImageSource = "Sly.jpeg"
                }
            };

            GameCharListView.ItemsSource = GameCharList;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            PopulateList();
            GameCharListView.IsRefreshing = false;
        }

        void Handle_DeleteGameChar(object sender, EventArgs e)
        { 
            var menuItem = (MenuItem)sender;
            var charDelete = (GameChar)menuItem.CommandParameter;
            GameCharList.Remove(charDelete);
        }

        private void Handle_CharInfo(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            GameChar gameCharInfo = (GameChar)menuItem.CommandParameter;
            if (gameCharInfo.Name == "Ratchet")
            {
                Navigation.PushAsync(new RatchetInfo());
            }
            else if (gameCharInfo.Name == "Sly Cooper")
            {
                Navigation.PushAsync(new SlyCooperInfo());
            }
            else if (gameCharInfo.Name == "Jak")
            {
                Navigation.PushAsync(new JakInfo());
            }
            else if (gameCharInfo.Name == "Crash")
            {
                Navigation.PushAsync(new CrashInfo());
            }
            else if (gameCharInfo.Name == "Spyro")
            {
                Navigation.PushAsync(new SpyroInfo());
            }
        }
    }
}
