﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4
{
    public partial class CrashInfo : ContentPage
    {
        public CrashInfo()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left the Crash page", "Ok");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Crash page", "Ok");
        }

        //Button that removes the button and displays the info about crash
        private void crash_click(object sender, EventArgs e)
        {

            Label label = crash_button_info;
            Label label1 = crash_description;
            Button removeButton = crash_button;

            removeButton.IsVisible = !removeButton.IsVisible;
            crash_button_info.IsVisible = !crash_button_info.IsVisible;
            crash_description.Text = "Crash Bandicoot is the title character and main protagonist of the Crash Bandicoot series. Crash is a mutant eastern barred bandicoot who was genetically enhanced by the series' main antagonist Doctor Neo Cortex and soon escaped from Cortex's castle after a failed experiment in the 'Cortex Vortex'.";
        }

        //Function to change a lot of colors when the button is clicked
        void text_click(object sender, EventArgs e)
        {
            Random rand_Color = new Random();
            crash_description.BackgroundColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
            crash_description.TextColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
            BackgroundColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
        }
    }
}