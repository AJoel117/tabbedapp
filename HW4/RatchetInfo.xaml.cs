﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4
{
    public partial class RatchetInfo : ContentPage
    {
        public RatchetInfo()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left the Ratchet page", "Ok");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Ratchet page", "Ok");
        }

        //This function removed the button and displays information about ratchet
        private void ratchet_click(object sender, EventArgs e)
        {

            Label label = ratchet_button_info;
            Label label1 = ratchet_description;
            Button removeButton = ratchet_button;

            removeButton.IsVisible = !removeButton.IsVisible;
            ratchet_button_info.IsVisible = !ratchet_button_info.IsVisible;
            ratchet_description.Text = "The plot arc focuses on Ratchet, a lombax mechanic with a lifelong desire to explore space, and Clank, a Zoni robot created in a robot factory, as they go on adventures to save the universe from various threats.";
        }

        //This function changes the text button color
        void text_click(object sender, EventArgs e)
        {
            Random rand_Color = new Random();
            text_color_change.TextColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
        }
    }
}