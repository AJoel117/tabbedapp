﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4
{
    public partial class JakInfo : ContentPage
    {
        public JakInfo()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left the Jak page", "Ok");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Jak page", "Ok");
        }

        //This function removes the button and displays the info about Jak
        private void jak_click(object sender, EventArgs e)
        {

            Label label = jak_button_info;
            Label label1 = jak_description;
            Button removeButton = jak_button;

            removeButton.IsVisible = !removeButton.IsVisible;
            jak_button_info.IsVisible = !jak_button_info.IsVisible;
            jak_description.Text = "The plot arc focuses on a human character named Jak and his ottsel sidekick Daxter, who are intertwined in an adventure involving the mystical Precursors. The games in the franchise span a variety of genres, primarily centered upon open world action adventure.";
        }

        //This function changes the color of the text when the button is clicked
        void text_click(object sender, EventArgs e)
        {
            Random rand_Color = new Random();
            jak_description.TextColor = Color.FromRgb(rand_Color.Next(256), rand_Color.Next(256), rand_Color.Next(256));
        }
    }
}