﻿using Xamarin.Forms;

namespace HW4
{
    public class GameChar
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public ImageSource ImageSource { get; set; }
    }
}